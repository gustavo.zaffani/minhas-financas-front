export class PageRequest {
  size: number;
  offset: number;
  fieldOrder: string;
  typeOrder: string;
}
