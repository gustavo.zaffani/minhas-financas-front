import {MessageService} from 'primeng/api';
import {PREFIX} from './utils';
import {TranslateService} from '@ngx-translate/core';

export class MessageUtil {

  public static showMessageSuccess(messageService: MessageService, translateService: TranslateService, keyMessage: string) {
    messageService.add(
      {
        severity: 'success',
        summary: translateService.instant(`${PREFIX}alert.success`),
        detail: translateService.instant(PREFIX + keyMessage)
      }
    );
  }

  public static showMessageError(messageService: MessageService, translateService: TranslateService, keyMessage: string) {
    messageService.add(
      {
        severity: 'error',
        summary: translateService.instant(`${PREFIX}alert.attention`),
        detail: translateService.instant(PREFIX + keyMessage)
      }
    );
  }
}
