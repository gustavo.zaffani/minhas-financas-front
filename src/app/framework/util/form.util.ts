import {FormGroup, Validators} from '@angular/forms';

export class FormUtil {

  public static enableField(field: string, formGroup: FormGroup) {
    formGroup.get(field).enable();
  }

  public static enableFields(fields: string[], formGroup: FormGroup) {
    fields.forEach(field => {
      this.enableField(field, formGroup);
    });
  }

  public static disableField(field: string, formGroup: FormGroup) {
    formGroup.get(field).disable();
  }

  public static disableFields(fields: string[], formGroup: FormGroup) {
    fields.forEach(field => {
      this.disableField(field, formGroup);
    });
  }

  public static clearField(field: string, formGroup: FormGroup) {
    formGroup.get(field).patchValue(null);
  }

  public static clearFields(fields: string[], formGroup: FormGroup) {
    fields.forEach(field => {
      this.clearField(field, formGroup);
    })
  }

  public static disableAndClearField(field: string, formGroup: FormGroup) {
    this.disableField(field, formGroup);
    this.clearField(field, formGroup);
  }

  public static disableAndClearFields(fields: string[], formGroup: FormGroup) {
    this.disableFields(fields, formGroup);
    this.clearFields(fields, formGroup);
  }

  public static fieldIsDisabled(field: string, formGroup: FormGroup) {
    return formGroup.get(field).disabled;
  }

  public static clearValidatorsFields(fields: string[], formGroup: FormGroup) {
    fields.forEach(field => {
      formGroup.get(field).clearValidators();
      formGroup.updateValueAndValidity();
    });
  }

  public static setValidatorRequiredFields(fields: string[], formGroup: FormGroup) {
    fields.forEach(field => {
      formGroup.get(field).setValidators(Validators.required);
      formGroup.updateValueAndValidity();
    });
  }
}
