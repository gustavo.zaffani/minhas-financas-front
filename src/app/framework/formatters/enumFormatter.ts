import {PipeFormatter} from '../component/table-custom/pipeFormatter';
import {SelectOptions} from '../component/filter-custom/selectOptions';

export class EnumFormatter {

  typeFormatter: PipeFormatter;
  options: SelectOptions[];

  constructor(options: SelectOptions[]) {
    this.typeFormatter = PipeFormatter.ENUM;
    this.options = options;
  }

  public static format(objectElement: any, formatter: any) {
    const option = formatter.options.filter(option => option.value === objectElement);
    if (option != null && option.length == 1) {
      return option[0].label;
    }
    return objectElement;
  }
}
