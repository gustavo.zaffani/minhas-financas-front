import {PipeFormatter} from '../component/table-custom/pipeFormatter';

export class CurrencyFormatter {

  typeFormatter: PipeFormatter;

  constructor() {
    this.typeFormatter = PipeFormatter.CURRENCY;
  }

  public static format(value: number): string {
    if (value) {
      return 'R$ ' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&.');
    }
    return '0';
  }
}
