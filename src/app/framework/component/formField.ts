import {Validators} from '@angular/forms';
import {FieldType} from './filter-custom/fieldType';

export class FormField {
  size: string;
  field: string;
  label: string;
  type: FieldType;
  variable?: any;
  required?: boolean;
  validators?: Validators[];

  // constructor(config: any) {
  //
  // };
}
