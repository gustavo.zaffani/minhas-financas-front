import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CaracterCoutingComponent} from './caracter-couting.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CaracterCoutingComponent
  ],
  exports: [
    CaracterCoutingComponent
  ]
})
export class CaracterCoutingModule {
}
