import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-caracter-couting',
  templateUrl: './caracter-couting.component.html',
  styleUrls: ['./caracter-couting.component.css']
})
export class CaracterCoutingComponent {

  @Input() valueToCouting: any[];
  @Input() maxSize: number;

  getTotalCaracteres() {
    if (this.valueToCouting) {
      return this.valueToCouting.length;
    }
    return 0;
  }
}
