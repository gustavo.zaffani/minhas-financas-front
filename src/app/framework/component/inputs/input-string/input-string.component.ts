import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-input-string',
  templateUrl: './input-string.component.html',
  styleUrls: ['./input-string.component.css']
})
export class InputStringComponent {

  @Input() size: number;
  @Input() nameField: string;
  @Input() label: string;
  @Input() required: boolean = false;
  @Input() value: string = null;

  constructor() {
  }
}
