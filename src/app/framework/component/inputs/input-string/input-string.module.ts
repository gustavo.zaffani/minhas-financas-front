import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputStringComponent} from './input-string.component';
import {InputTextModule} from 'primeng/inputtext';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ValidationModule} from '../../../validation/validation.module';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    ReactiveFormsModule,
    FormsModule,
    ValidationModule
  ],
  declarations: [
    InputStringComponent
  ],
  exports: [
    InputStringComponent
  ]
})
export class InputStringModule {}
