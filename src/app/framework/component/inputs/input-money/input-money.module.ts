import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputMoneyComponent} from './input-money.component';
import {ValidationModule} from '../../../validation/validation.module';
import {OnlyNumberModule} from '../../../directives/onlyNumber/onlyNumber.module';
import {InputTextModule} from 'primeng/inputtext';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ValidationModule,
    OnlyNumberModule,
    InputTextModule,
    CurrencyMaskModule
  ],
  declarations: [
    InputMoneyComponent
  ],
  exports: [
    InputMoneyComponent
  ]
})
export class InputMoneyModule {
}
