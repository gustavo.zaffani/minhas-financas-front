import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-input-money',
  templateUrl: './input-money.component.html',
  styleUrls: ['./input-money.component.css']
})
export class InputMoneyComponent implements OnInit {

  @Input() size: number;
  @Input() nameField: number;
  @Input() label: number;
  @Input() required: boolean = false;
  @Input() value: number;
  @Output() valueChange: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    console.log(this.value);
  }

  setValue() {
    console.log(this.value);
    this.valueChange.emit(this.value);
  }
}
