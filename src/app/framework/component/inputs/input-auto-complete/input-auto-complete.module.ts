import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputAutoCompleteComponent} from './input-auto-complete.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    InputAutoCompleteComponent
  ],
  exports: [
    InputAutoCompleteComponent
  ]
})
export class InputAutoCompleteModule {
}
