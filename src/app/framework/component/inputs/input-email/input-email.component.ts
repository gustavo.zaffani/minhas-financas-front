import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-input-email',
  templateUrl: './input-email.component.html',
  styleUrls: ['./input-email.component.css']
})
export class InputEmailComponent implements OnInit {

  @Input() size: number;
  @Input() nameField: string;
  @Input() label: string;
  @Input() required: boolean = false;
  @Input() value: string = null;

  constructor() { }

  ngOnInit(): void {
  }

}
