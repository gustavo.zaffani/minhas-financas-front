import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputEmailComponent} from './input-email.component';
import {EmailValidatorModule} from '../../../validator/email/email.validator.module';
import {ValidationModule} from '../../../validation/validation.module';

@NgModule({
  imports: [
    CommonModule,
    EmailValidatorModule,
    ValidationModule
  ],
  declarations: [
    InputEmailComponent
  ],
  exports: [
    InputEmailComponent
  ]
})
export class InputEmailModule {
}
