import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-input-number',
  templateUrl: './input-number.component.html',
  styleUrls: ['./input-number.component.css']
})
export class InputNumberComponent implements OnInit {

  @Input() size: number;
  @Input() nameField: number;
  @Input() label: number;
  @Input() required: boolean = false;
  @Input() value: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
