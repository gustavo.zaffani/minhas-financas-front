import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputNumberComponent} from './input-number.component';
import {OnlyNumberModule} from '../../../directives/onlyNumber/onlyNumber.module';
import {ReactiveFormsModule} from '@angular/forms';
import {ValidationModule} from '../../../validation/validation.module';

@NgModule({
  imports: [
    CommonModule,
    OnlyNumberModule,
    ReactiveFormsModule,
    ValidationModule
  ],
  declarations: [
    InputNumberComponent
  ],
  exports: [
    InputNumberComponent
  ]
})
export class InputNumberModule {

}
