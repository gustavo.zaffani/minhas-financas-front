import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputPasswordComponent} from './input-password.component';
import {PasswordModule} from 'primeng/password';
import {ValidationModule} from '../../../validation/validation.module';

@NgModule({
  imports: [
    CommonModule,
    PasswordModule,
    ValidationModule
  ],
  declarations: [
    InputPasswordComponent
  ],
  exports: [
    InputPasswordComponent
  ]
})
export class InputPasswordModule {}
