import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-input-password',
  templateUrl: './input-password.component.html',
  styleUrls: ['./input-password.component.css']
})
export class InputPasswordComponent implements OnInit {

  @Input() size: number;
  @Input() nameField: string;
  @Input() label: string;
  @Input() required: boolean = false;
  @Input() value: string = null;
  @Input() minLength: number = 0;
  @Input() maxLength: number = 20;

  constructor() { }

  ngOnInit(): void {
  }
}
