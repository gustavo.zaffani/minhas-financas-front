import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputDateComponent} from './input-date.component';
import {CalendarModule} from 'primeng/calendar';
import {FormsModule} from '@angular/forms';
import {ValidationModule} from '../../../validation/validation.module';

@NgModule({
  imports: [
    CommonModule,
    CalendarModule,
    FormsModule,
    ValidationModule
  ],
  declarations: [
    InputDateComponent
  ],
  exports: [
    InputDateComponent
  ],
  providers: []
})
export class InputDateModule {
}
