import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {Calendar} from 'primeng/calendar';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  styleUrls: ['./input-date.component.css']
})
export class InputDateComponent implements OnInit, OnChanges {

  @ViewChild('inputDate') inputDate: Calendar;
  @Input() size: number;
  @Input() nameField: string;
  @Input() label: string;
  @Input() required: boolean = false;
  @Input() value: string;
  @Output() valueChange: EventEmitter<string> = new EventEmitter();
  private dateIsChangedOnEdit: boolean = false;

  constructor() {
  }

  ngOnInit(): void {
    console.log(this.value);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!this.dateIsChangedOnEdit) {
      this.setValueDate(changes.value.currentValue);
    }
  }

  getValueDate(event: any) {
    this.value = new Date(event).toLocaleDateString();
    this.valueChange.emit(this.value);
  }

  setValueDate(valueDate: string) {
    if (valueDate && valueDate != undefined) {
      this.inputDate.updateModel(this.getDateParsed(valueDate));
      this.inputDate.updateInputfield();
      this.dateIsChangedOnEdit = true;
    }
  }

  getDateParsed(dateString: string): Date {
    const dateSplited = dateString.split('/');
    const year = Number(dateSplited[2]);
    const month = Number(dateSplited[1]) - 1;
    const day = Number(dateSplited[0]);
    return new Date(year, month, day);
  }
}
