import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableCustomComponent} from './table-custom.component';
import {TableModule} from 'primeng/table';
import {RouterModule} from '@angular/router';
import {ButtonModule} from 'primeng/button';
import {EmptyTableModule} from '../emptyTable/emptyTable.module';
import {TableStyleService} from './table-style.service';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    RouterModule,
    ButtonModule,
    EmptyTableModule
  ],
  declarations: [
    TableCustomComponent
  ],
  exports: [
    TableCustomComponent
  ],
  providers: [
    TableStyleService
  ]
})
export class TableCustomModule {

}
