import {TextAlign} from './textAlign';
import {FontWeight} from './fontWeight';
import {Badge} from './badge';

export class TableColumn {
  field: string;
  header: string;
  formatter?: any;
  colorText?: string;
  textAlign?: TextAlign;
  fontWeight?: FontWeight;
  badge?: Badge;
}
