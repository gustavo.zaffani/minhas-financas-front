import {Injectable} from '@angular/core';
import {TableColumn} from './tableColumn';
import {FontWeight} from './fontWeight';
import {TextAlign} from './textAlign';
import {BadgeColor} from './badgeColor';
import {Badge} from './badge';

@Injectable()
export class TableStyleService {

  /**
   * Function responsável por montar styles, de acordo com valores passados no tableColumn
   * @param value -> valor do campo que será estilizado (caso houver estilização)
   * @param tableColumn -> dados informado na criação do campo, com dados da estilização
   */
  public buildStyles(value: any, tableColumn: TableColumn): string {
    let styles = '';
    styles = styles + ' ' + this.getColorText(tableColumn.colorText);
    return styles;
  }

  /**
   * Function responsável por montar classes de estilização, de acordo com valores passados no tableColumn
   * @param value -> valor do campo que será estilizado (caso houver estilização)
   * @param tableColumn -> dados informado na criação do campo, com dados da estilização
   */
  public buildClasses(value: any, tableColumn: TableColumn): string {
    let classes = '';
    classes = classes + ' ' + this.getFontWeight(tableColumn.fontWeight);
    classes = classes + ' ' + this.getTextAlign(tableColumn.textAlign);
    classes = classes + ' ' + this.getBadge(tableColumn.badge, value);
    classes = classes + ' ' + this.getClassToValueNull(value);
    return classes;
  }

  private getBadge(badge: Badge, valueField: any) {
    if (badge != null) {
      if (badge.badgeColor != null) {
        return this.getClassBadgeByBadgeColor(badge.badgeColor);
      } else {
        const badgeResult = badge.badgeOptions.filter(value => value.onlyResult === valueField);
        if (badgeResult != null && badgeResult.length == 1) {
          return this.getClassBadgeByBadgeColor(badgeResult[0].badge);
        }
      }
    }
    return '';
  }

  private getClassBadgeByBadgeColor(badgeColor: BadgeColor) {
    switch (badgeColor) {
      case BadgeColor.BLUE:
        return 'badge badge-primary';
      case BadgeColor.YELLOW:
        return 'badge badge-warning';
      case BadgeColor.GREEN:
        return 'badge badge-success';
      case BadgeColor.RED:
        return 'badge badge-danger';
      default:
        return '';
    }
  }

  private getFontWeight(fontWeight: FontWeight): string {
    if (fontWeight != null) {
      switch (fontWeight) {
        case FontWeight.BOLD:
          return 'text-bold';
        case FontWeight.BOLDER:
          return 'text-bolder';
        default:
          return '';
      }
    }
    return '';
  }

  private getTextAlign(textAlign: TextAlign): string {
    if (textAlign != null) {
      switch (textAlign) {
        case TextAlign.CENTER:
          return 'text-center';
        case TextAlign.LEFT:
          return 'text-left';
        case TextAlign.RIGHT:
          return 'text-right';
        default:
          return '';
      }
    }
    return '';
  }

  private getClassToValueNull(value: any) {
    if (!value) {
      return 'field-null';
    }
  }

  private getColorText(colorText: string): string {
    if (colorText != null) {
      return 'color: ' + colorText;
    }
  }
}
