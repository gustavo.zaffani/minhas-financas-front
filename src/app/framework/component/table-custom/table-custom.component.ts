import {AfterViewInit, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {PageRequest} from '../../dto/pageRequest';
import {EnumTypeOrder} from '../../enum/enumTypeOrder';
import {ConfirmationService} from 'primeng/api';
import {CurrencyFormatter} from '../../formatters/currencyFormatter';
import {TableColumn} from './tableColumn';
import {PipeFormatter} from './pipeFormatter';
import {TableStyleService} from './table-style.service';
import {EnumFormatter} from '../../formatters/enumFormatter';

@Component({
  selector: 'app-table-custom',
  templateUrl: './table-custom.component.html',
  styleUrls: ['./table-custom.component.css']
})
export class TableCustomComponent implements AfterViewInit {

  @ViewChild('table') table: Table;
  @Input() objectList: any[];
  @Input() totalElements: number = 0;
  @Input() columns: TableColumn[];
  @Input() showButtonNew = true;
  @Input() showButtonEdit = true;
  @Input() showButtonDelete = true;
  @Input() showTableCheckbox = true;
  @Input() sortIsActive = true;
  @Input() navigateToFormOnAdd = true;
  @Input() paginatorIsActive = true;
  @Output() onTableChanged: EventEmitter<PageRequest> = new EventEmitter();
  @Output() onAddObject: EventEmitter<any> = new EventEmitter();
  @Output() onEditObject: EventEmitter<any[]> = new EventEmitter();
  @Output() onDeleteObject: EventEmitter<any[]> = new EventEmitter();
  public selectedObjects: any[];
  public pageRequest: PageRequest;


  constructor(private confirmationService: ConfirmationService,
              private tableStyleService: TableStyleService) {
    this.pageRequest = new PageRequest();
  }

  ngAfterViewInit(): void {
    this.buildEventTable();
    this.setSizeAndOffsetPage();
    this.onTableChanged.emit(this.pageRequest);
  }

  buildEventTable() {
    if (this.sortIsActive) {
      this.table.onSort.subscribe((event) => {
        this.pageRequest.fieldOrder = event.field;
        this.pageRequest.typeOrder = this.getOrderSort(event);

        this.setSizeAndOffsetPage();
        this.onTableChanged.emit(this.pageRequest);
      });
    }

    if (this.paginatorIsActive) {
      this.table.onPage.subscribe(() => {
        this.setSizeAndOffsetPage();
        this.onTableChanged.emit(this.pageRequest);
      });
    }
  }

  getOrderSort(event) {
    if (event.order === 1) {
      return EnumTypeOrder.ASC;
    } else {
      return EnumTypeOrder.DESC;
    }
  }

  setSizeAndOffsetPage() {
    this.pageRequest.size = this.table.rows;
    this.pageRequest.offset = this.table.first / this.table.rows;
  }

  verifyDisableEdit() {
    if (this.selectedObjects == undefined
      || this.selectedObjects.length == 0
      || this.selectedObjects.length > 1) {
      return true;
    }
    return false;
  }

  verifyDisableDelete() {
    return this.selectedObjects == undefined || this.selectedObjects.length == 0;
  }

  addObject() {
    if (!this.navigateToFormOnAdd) {
      this.onAddObject.emit();
    }
  }

  editObject() {
    this.onEditObject.emit(this.selectedObjects);
  }

  deleteObject() {
    this.confirmationService.confirm({
        message: 'Tem certeza que deseja excluir o(s) registro(s) selecionado(s) ?',
        accept: () => {
          this.onDeleteObject.emit(this.selectedObjects);
          this.selectedObjects = new Array();
        }
      }
    );
  }

  getValue(object: any, column: TableColumn) {
    const value = this.getValueObject(object, column.field);
    if (value) {
      if (column.formatter != null) {
        return this.getObjectFormatted(value, column.formatter);
      }
      return value;
    }
    return 'Não informado';
  }

  getObjectFormatted(objectElement: any, formatter: any) {
    switch (formatter.typeFormatter) {
      case PipeFormatter.CURRENCY:
        return CurrencyFormatter.format(objectElement);
      case PipeFormatter.ENUM:
        return EnumFormatter.format(objectElement, formatter);
      default:
        return objectElement;
    }
  }

  getClasses(object: any, col: TableColumn) {
    const value = this.getValueObject(object, col.field);
    return this.tableStyleService.buildClasses(value, col);
  }

  getStyles(object: any, col: TableColumn) {
    const value = this.getValueObject(object, col.field);
    return this.tableStyleService.buildStyles(value, col);
  }

  getValueObject(object: any, field: string) {
    const fields = field.split('.');
    let valueReturn = object[fields[0]];
    if (fields.length > 1) {
      for (let i = 1; i < fields.length; i++) {
        valueReturn = valueReturn[fields[i]];
      }
    }
    return valueReturn;
  }
}
