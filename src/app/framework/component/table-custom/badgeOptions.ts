import {BadgeColor} from './badgeColor';

export class BadgeOptions {
  onlyResult: any;
  badge: BadgeColor;
}
