import {BadgeColor} from './badgeColor';
import {BadgeOptions} from './badgeOptions';

export class Badge {
  badgeColor?: BadgeColor;
  badgeOptions?: BadgeOptions[];
}
