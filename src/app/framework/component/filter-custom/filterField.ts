import {FieldType} from './fieldType';
import {Validators} from '@angular/forms';
import {SelectOptions} from './selectOptions';

export class FilterField {
  size: string;
  field: string;
  label: string;
  type: FieldType;
  required?: boolean;
  validators?: Validators[];
  options?: SelectOptions[];
}
