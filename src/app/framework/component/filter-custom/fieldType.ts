export enum FieldType {
  TEXT = 'text',
  NUMBER = 'number',
  MONEY = 'money',
  SELECT = 'select',
  // os tipos abaixos não serão utilizados no momento
  DATE = 'date',
  PASSWORD = 'password',
  EMAIL = 'email'
}
