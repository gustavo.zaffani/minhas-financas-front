import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilterCustomComponent} from './filter-custom.component';
import {AccordionModule} from 'primeng/accordion';
import {ReactiveFormsModule} from '@angular/forms';
import {OnlyNumberModule} from '../../directives/onlyNumber/onlyNumber.module';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';

@NgModule({
    imports: [
        CommonModule,
        AccordionModule,
        ReactiveFormsModule,
        OnlyNumberModule,
        CurrencyMaskModule,
        InputTextModule,
        DropdownModule
    ],
  declarations: [
    FilterCustomComponent
  ],
  exports: [
    FilterCustomComponent
  ]
})
export class FilterCustomModule {

}
