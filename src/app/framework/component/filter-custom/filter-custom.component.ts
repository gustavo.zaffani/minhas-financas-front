import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FilterField} from './filterField';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-filter-custom',
  templateUrl: './filter-custom.component.html',
  styleUrls: ['./filter-custom.component.css']
})
export class FilterCustomComponent implements OnInit {

  @Input() filterFields: FilterField[];
  @Input() nameEntity: string;
  @Output() onFilter: EventEmitter<FormGroup> = new EventEmitter();
  @Output() onClearFilter: EventEmitter<any> = new EventEmitter();
  form: FormGroup = new FormGroup({});
  selectedAccordionFilter = false;

  constructor() {
  }

  ngOnInit(): void {
    this.getFieldFormGroup();
    this.checkRequiredFields();
    this.verifyFilterSaved();
  }

  getFieldFormGroup() {
    if (this.filterFields) {
      this.filterFields.forEach(value => {
        let control: FormControl = new FormControl(null);
        this.form.addControl(value.field, control);
      });
    }
  }

  filter() {
    if (this.form.valid) {
      this.saveFilterOnLocalStorage();
      this.closeAccordionTab();
      this.onFilter.emit(this.form);
    }
  }

  clearFilter() {
    this.form.reset();
    this.clearFilterOnLocalStorage();
    this.closeAccordionTab();
    this.onClearFilter.emit();
  }

  saveFilterOnLocalStorage() {
    localStorage.setItem(this.getNameLocalStorage(), JSON.stringify(this.form.getRawValue()));
  }

  clearFilterOnLocalStorage() {
    localStorage.removeItem(this.getNameLocalStorage());
  }

  private checkRequiredFields() {
    if (this.nameEntity === null || this.nameEntity === undefined) {
      throw new Error('Atributo \'nameEntity\' é obrigatório');
    }
  }

  private verifyFilterSaved() {
    const filterSaved = JSON.parse(localStorage.getItem(this.getNameLocalStorage()));
    if (filterSaved && filterSaved !== undefined) {
      this.form.patchValue(filterSaved);
      this.onFilter.emit(this.form);
    }
  }

  private getNameLocalStorage() {
    return `filter_entity_${this.nameEntity}`;
  }

  private closeAccordionTab() {
    this.selectedAccordionFilter = false;
  }
}
