import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CancelarComponent} from './cancelar.component';
import {TooltipModule} from 'primeng/tooltip';
import {ButtonModule} from 'primeng/button';

@NgModule({
  imports: [
    CommonModule,
    TooltipModule,
    ButtonModule
  ],
  declarations: [
    CancelarComponent
  ],
  exports: [
    CancelarComponent
  ]
})
export class CancelarModule {

}
