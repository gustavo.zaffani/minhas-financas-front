import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SalvarComponent} from './salvar.component';
import {TooltipModule} from 'primeng/tooltip';
import {ButtonModule} from 'primeng/button';

@NgModule({
  imports: [
    CommonModule,
    TooltipModule,
    ButtonModule
  ],
  declarations: [
    SalvarComponent
  ],
  exports: [
    SalvarComponent
  ]
})
export class SalvarModule {

}
