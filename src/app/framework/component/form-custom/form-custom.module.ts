import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormCustomComponent} from './form-custom.component';
import {SalvarModule} from '../shared-button/salvar/salvar.module';
import {CancelarModule} from '../shared-button/cancelar/cancelar.module';
import {InputStringModule} from '../inputs/input-string/input-string.module';
import {InputDateModule} from '../inputs/input-date/input-date.module';
import {InputNumberModule} from '../inputs/input-number/input-number.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ValidationModule} from '../../validation/validation.module';
import {InputEmailModule} from '../inputs/input-email/input-email.module';
import {InputPasswordModule} from '../inputs/input-password/input-password.module';
import {EmailValidatorModule} from '../../validator/email/email.validator.module';
import {InputMoneyModule} from '../inputs/input-money/input-money.module';

@NgModule({
    imports: [
        CommonModule,
        SalvarModule,
        CancelarModule,
        InputStringModule,
        InputDateModule,
        InputNumberModule,
        ReactiveFormsModule,
        FormsModule,
        ValidationModule,
        InputEmailModule,
        InputPasswordModule,
        EmailValidatorModule,
        InputMoneyModule
    ],
  declarations: [
    FormCustomComponent
  ],
  exports: [
    FormCustomComponent
  ]
})
export class FormCustomModule {
}
