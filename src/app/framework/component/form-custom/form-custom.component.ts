import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormField} from '../formField';
import {FieldType} from '../filter-custom/fieldType';
import {BaseFormComponent} from '../../directives/base.form.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-form-custom',
  templateUrl: './form-custom.component.html',
  styleUrls: ['./form-custom.component.css']
})
export class FormCustomComponent extends BaseFormComponent implements OnInit {

  @Input() urlList: string;
  @Input() formFieldList: FormField[];
  @Input() objectEntity: any;
  @Input() onEdit: EventEmitter<any> = new EventEmitter();
  @Output() objectEntityChange: EventEmitter<any> = new EventEmitter();
  @Output() onSave: EventEmitter<void> = new EventEmitter();

  constructor(private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.buildEvents();
  }

  buildEvents() {
    this.onEdit.asObservable().subscribe(value => {
      this.objectEntity = value;
      this.formFieldList.forEach(field => {
        if (this.objectEntity[field.field]) {
          field.variable = this.objectEntity[field.field];
        }
      });
      console.log(this.formFieldList);
    });
  }

  save() {
    console.log(this.formFieldList);
    if (this.isValid()) {
      this.formFieldList.forEach(field => {
        this.objectEntity[field.field] = field.variable;
      });
      this.objectEntityChange.emit(this.objectEntity);
      console.log(this.objectEntity);
      this.onSave.emit();
    } else {
      console.log('not valid');
      this.validarFormulario();
    }
  }

  isInputString(fieldType: FieldType) {
    return fieldType === FieldType.TEXT;
  }

  isInputNumber(fieldType: FieldType) {
    return fieldType === FieldType.NUMBER;
  }

  isInputMoney(fieldType: FieldType) {
    return fieldType === FieldType.MONEY;
  }

  isInputDate(fieldType: FieldType) {
    return fieldType === FieldType.DATE;
  }

  isInputPassword(fieldType: FieldType) {
    return fieldType === FieldType.PASSWORD;
  }

  isInputEmail(fieldType: FieldType) {
    return fieldType === FieldType.EMAIL;
  }

  backToList() {
    this.router.navigate([this.urlList]);
  }
}
