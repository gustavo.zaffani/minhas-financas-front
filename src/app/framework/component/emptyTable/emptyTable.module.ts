import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EmptyTableComponent} from './emptyTable.component';
import {RouterModule} from '@angular/router';
import {ButtonModule} from 'primeng/button';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ButtonModule
  ],
  declarations: [
    EmptyTableComponent
  ],
  exports: [
    EmptyTableComponent
  ]
})
export class EmptyTableModule{}
