import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-empty-table',
  templateUrl: './emptyTable.component.html',
  styleUrls: ['./emptyTable.component.css']
})
export class EmptyTableComponent {

  @Input() dataSource: any[];
  @Input() navigateToFormOnAdd = true;
  @Input() idDivTable: string = 'table-custom';
  @Input() title: string = 'Nenhum resultado encontrado';
  @Input() subtitle: string = 'Caso tenha utilizado o filtro, favor realizar a pesquisa novamente.';
  @Input() showSubtitle: boolean = false;
  @Input() routerLinkToForm: string = 'form';
  @Output() onAddObject: EventEmitter<any> = new EventEmitter();

  showMessageEmptyTable() {
    if (this.dataSource && this.dataSource.length > 0) {
      this.toogleTable(true);
      return false;
    }
    this.toogleTable(false);
    return true;
  }

  toogleTable(show) {
    if (show) {
      document.getElementById(this.idDivTable).classList.remove('d-none');
    } else {
      document.getElementById(this.idDivTable).classList.add('d-none');
    }
  }

  addNewObject() {
    if (!this.navigateToFormOnAdd) {
      this.onAddObject.emit();
    }
  }
}
