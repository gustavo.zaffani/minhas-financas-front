import {ActivatedRoute, Router} from '@angular/router';
import {Directive, Injector, OnInit} from '@angular/core';
import {MessageService} from 'primeng/api';
import {CrudService} from '../service/crud.service';
import {PREFIX, Utils} from '../util/utils';
import {LoaderService} from '../loader/loader.service';
import {FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {MessageUtil} from '../util/message.util';

@Directive()
export abstract class CrudFormComponent<T, I, O, ID> implements OnInit {

  protected router: Router;
  protected messageService: MessageService;
  protected route: ActivatedRoute;
  protected loaderService: LoaderService;
  protected translateService: TranslateService;
  protected formPrefix: string;
  public formGroup: FormGroup;

  constructor(protected service: CrudService<T, I, O, ID>,
              protected injector: Injector,
              protected urlList: string) {
    this.router = this.injector.get(Router);
    this.route = this.injector.get(ActivatedRoute);
    this.messageService = this.injector.get(MessageService);
    this.loaderService = this.injector.get(LoaderService);
    this.translateService = this.injector.get(TranslateService);
  }

  ngOnInit(): void {
    this.preOnInit();
    this.route.params.subscribe(params => {
      if (params.id) {
        if (Utils.isBlank(params.id)) {
          this.initializeValues();
        } else {
          this.edit(params.id);
        }
      } else {
        this.initializeValues();
      }
    });
    this.postOnInit();
  }

  save() {
    if (this.formGroup.valid) {
      this.loaderService.display(true);
      this.service.save(this.formGroup.getRawValue())
        .subscribe(e => {
          this.loaderService.display(false);
          MessageUtil.showMessageSuccess(this.messageService, this.translateService, 'message.saved.success');
          this.back();
        }, error => {
          console.error(error);
          this.loaderService.display(false);
          MessageUtil.showMessageError(this.messageService, this.translateService, 'message.saved.error');
        });
    } else {
      this.formGroup.markAllAsTouched();
    }
  }

  initializeValues(): void {
  }

  preOnInit(): void {
  }

  postOnInit(): void {
  }

  postEdit(): void {
  }

  edit(id: ID) {
    this.loaderService.display(true);
    this.service.findOne(id)
      .subscribe(e => {
        this.formGroup.patchValue(e);
        this.loaderService.display(false);
        this.postEdit();
      }, error => {
        this.loaderService.display(false);
        console.error('error');
      });
  }

  back() {
    this.router.navigate([this.urlList]);
  }

  createFormPrefix(form: string) {
    this.formPrefix = `${PREFIX + form}_form_`;
  }

  translate(formField: string) {
    return this.translateService.instant(`${this.formPrefix + formField}`);
  }
}
