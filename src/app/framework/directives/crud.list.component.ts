import {Directive, Injector, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ConfirmationService, MessageService} from 'primeng/api';
import {CrudService} from '../service/crud.service';
import {PageRequest} from '../dto/pageRequest';
import {FilterField} from '../component/filter-custom/filterField';
import {TranslateService} from '@ngx-translate/core';
import {LoaderService} from '../loader/loader.service';
import {TableColumn} from '../component/table-custom/tableColumn';
import {PREFIX} from '../util/utils';

/**
 * O 'T' é o genérico para a classe principal da listagem
 * O 'I' é o genérico para a classe de input da listagem
 * O 'O' é o genérico para a classe de output da listagem
 * O 'ID' é o tipo da chave primária da classe principal (No caso desse projeto, será string, pois é um UUID)
 * */
@Directive()
export abstract class CrudListComponent<T, I, O, ID> implements OnInit {

  protected router: Router;
  protected messageService: MessageService;
  protected confirmationService: ConfirmationService;
  protected translateService: TranslateService;
  protected loaderService: LoaderService;
  public filterFields: FilterField[];
  public columns: TableColumn[];
  public pageRequest: PageRequest;
  public totalElements: number = 0;
  public objectInput: I;
  public objectList: T[];
  private listPrefix: string;

  protected constructor(protected service: CrudService<T, I, O, ID>,
                        protected injector: Injector,
                        protected urlForm: string) {
    this.router = this.injector.get(Router);
    this.messageService = this.injector.get(MessageService);
    this.confirmationService = this.injector.get(ConfirmationService);
    this.translateService = this.injector.get(TranslateService);
    this.loaderService = this.injector.get(LoaderService);
  }

  ngOnInit(): void {
    this.newInstances();
    this.getColumnsGrid();
    this.getFilterFields();

  }

  edit(id: string) {
    let idToEdit = {
      id: id
    };
    this.router.navigate([this.urlForm, idToEdit]);
  }

  delete(objectsToExclude: any[]) {
    this.loaderService.display(true);
    this.service.deleteObjects(this.getIdsToExclude(objectsToExclude))
      .subscribe(e => {
        this.loaderService.display(false);
        this.findAllByFilter();
        this.messageService.add({severity: 'success', summary: 'Sucesso!', detail: 'Registro(s) removido(s) com sucesso.'});
      }, error => {
        this.loaderService.display(false);
        this.messageService.add({severity: 'error', summary: 'Atenção!', detail: 'Ocorreu um erro ao remover o(s) registro(s).'});
      });
  }

  findAllByFilter() {
    console.log(this.objectInput);
    this.loaderService.display(true);
    // @ts-ignore
    this.objectInput.pageRequest = this.pageRequest;
    this.service.findAllByFilter(this.objectInput)
      .subscribe(data => {
        if (data) {
          this.loaderService.display(false);
          // @ts-ignore
          this.objectList = data.contents;
          // @ts-ignore
          this.totalElements = data.totalElements;
        }
      }, error => {
        this.loaderService.display(false);
      });
  }

  getFilterFields() {
  }

  getColumnsGrid() {
  }

  onTableChanged($event: PageRequest) {
    this.pageRequest = $event;
    this.findAllByFilter();
  }

  getIdsToExclude(objectsToExclude: any[]) {
    let idsToReturn = new Array();
    objectsToExclude.forEach(value => {
      idsToReturn.push(value.id);
    });
    return idsToReturn;
  }

  newInstances() {
    this.objectInput = {} as I;
    this.objectList = new Array();
    this.filterFields = new Array();
  }

  createListPrefix(form: string) {
    this.listPrefix = `${PREFIX + form}_list_`;
  }

  translate(formField: string) {
    return this.translateService.instant(`${this.listPrefix + formField}`);
  }

  clearFilter() {
    this.objectInput = {} as I;
    // @ts-ignore
    this.objectInput.pageRequest = this.pageRequest;
    this.findAllByFilter();
  }
}
