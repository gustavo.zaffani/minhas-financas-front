import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DeleteInput} from '../dto/deleteInput';

export abstract class CrudService<T, I, O, ID> {

  constructor(protected url: string, protected http: HttpClient) {
  }

  protected getUrl(): string {
    return this.url;
  }

  findAll(): Observable<T[]> {
    return this.http.get<T[]>(this.getUrl());
  }

  findAllByFilter(inputFilter: I): Observable<O> {
    return this.http.post<O>(this.getUrl() + 'findAllByFilter', inputFilter);
  }

  findOne(id: ID): Observable<T> {
    return this.http.get<T>(this.getUrl() + id);
  }

  save(t: T): Observable<T> {
    return this.http.post<T>(this.getUrl(), t);
  }

  delete(id: ID): Observable<void> {
    return this.http.delete<void>(`${this.url + id}`);
  }

  findObjectsByFilter(filter: I): Observable<O> {
    return this.http.post<O>(`${this.getUrl()}filter`, filter);
  }

  deleteObjects(id: ID[]): Observable<void> {
    return this.http.post<void>(`${this.url}deleteObjects`, new DeleteInput(id));
  }

  complete(query: string): Observable<T[]> {
    return this.http.get<T[]>(`${this.url}complete?query=${query}`);
  }
}
