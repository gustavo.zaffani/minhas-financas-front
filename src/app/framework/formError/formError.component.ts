import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {StringUtils} from '../util/string.utils';

@Component({
  selector: 'app-form-error',
  templateUrl: './formError.component.html',
  styleUrls: ['./formError.component.css']
})
export class FormErrorComponent {

  @Input() form: FormGroup;
  @Input() inputName: string;
  @Input() messageRequired = "Campo obrigatório";
  @Input() messageMin: string;
  @Input() messageMax: string;

  @Input() minValue = 0.01;
  @Input() maxValue: number;

  getMessageError() {
    const errors = this.form.get(this.inputName).errors;
    if (errors.required) {
      return this.messageRequired;
    } else if (errors.min) {
      return this.getFormatMessageMinValue();
    } else if (errors.max) {
      return this.getFormatMessageMaxValue();
    }
    return null;
  }

  private getFormatMessageMinValue() {
    if (StringUtils.isNotBlank(this.messageMin)) {
      return StringUtils.formatValue(this.messageMin, this.minValue);
    }
    return `O valor mínimo é ${this.minValue}`;
  }

  private getFormatMessageMaxValue() {
    if (StringUtils.isNotBlank(this.messageMax)) {
      return StringUtils.formatValue(this.messageMax, this.maxValue);
    }
    return `O valor máximo é ${this.maxValue}`;
  }
}
