import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MenuModule} from './geral/menu/menu.module';
import {ToolbarrModule} from './geral/toolbar/toolbarr.module';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {CategoriaModule} from './categoria/categoria.module';
import {ConfirmationService, MessageService} from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import {ValidationService} from './framework/validation/validation.service';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {UsuarioModule} from './usuario/usuario.module';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {LoaderModule} from './framework/loader/loader.module';
import {LoaderService} from './framework/loader/loader.service';
import {CartaoModule} from './cartao/cadastro/cartao.module';
import {CURRENCY_MASK_CONFIG} from 'ng2-currency-mask';
import {CustomCurrencyMaskConfig} from './framework/constants/currency.mask.config';
import {NotFoundModule} from './geral/notFound/notFound.module';
import {PessoaModule} from './pessoa/pessoa.module';
import {DespesaFixaModule} from './despesa-fixa/despesa-fixa.module';
import {HomeModule} from './home/home.module';
import {DespesaVariavelModule} from './despesa-variavel/despesa-variavel.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MenuModule,
    ToolbarrModule,
    ScrollPanelModule,
    CategoriaModule,
    ToastModule,
    ConfirmDialogModule,
    UsuarioModule,
    CartaoModule,
    NotFoundModule,
    PessoaModule,
    DespesaFixaModule,
    HomeModule,
    DespesaVariavelModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
    LoaderModule,
  ],
  exports: [
    TranslateModule
  ],
  providers: [
    MessageService,
    ConfirmationService,
    ValidationService,
    LoaderService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFactory,
      deps: [TranslateService],
      multi: true
    },
    {
      provide: LOCALE_ID,
      useValue: 'pt'
    },
    {
      provide: CURRENCY_MASK_CONFIG,
      useValue: CustomCurrencyMaskConfig,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function httpTranslateLoader(http: HttpClient): any {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

export function appInitializerFactory(translate: TranslateService) {
  return () => {
    translate.setDefaultLang('pt-BR');
    return translate.use('pt-BR').toPromise();
  };
}
