import {Component, Injector} from '@angular/core';
import {CrudFormComponent} from '../../framework/directives/crud.form.component';
import {Categoria} from '../categoria';
import {CategoriaService} from '../categoria.service';
import {CategoriaInput} from '../categoriaInput';
import {CategoriaOutput} from '../categoriaOutput';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-categoria-form',
  templateUrl: './categoria.form.component.html',
  styleUrls: ['./categoria.form.component.css']
})
export class CategoriaFormComponent extends CrudFormComponent<Categoria, CategoriaInput, CategoriaOutput, string> {

  constructor(protected categoriaService: CategoriaService,
              protected injector: Injector) {
    super(categoriaService, injector, '/categoria');
    this.getFormGroup();
    this.createFormPrefix('categoria');
  }

  private getFormGroup() {
    this.formGroup = new FormBuilder().group({
      id: [null],
      codigo: [null, Validators.required],
      descricao: [null, Validators.required]
    });
  }
}
