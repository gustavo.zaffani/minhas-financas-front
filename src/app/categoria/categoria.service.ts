import {Injectable} from '@angular/core';
import {CrudService} from '../framework/service/crud.service';
import {Categoria} from './categoria';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {CategoriaOutput} from './categoriaOutput';
import {CategoriaInput} from './categoriaInput';

@Injectable()
export class CategoriaService extends CrudService<Categoria, CategoriaInput, CategoriaOutput, string> {

  constructor(http: HttpClient) {
    super(`${environment.api_url}categoria/`, http);
  }
}
