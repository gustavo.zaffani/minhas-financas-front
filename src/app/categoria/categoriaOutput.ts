import {Categoria} from './categoria';

export class CategoriaOutput {
  contents: Categoria[];
  totalElements: number;
}
