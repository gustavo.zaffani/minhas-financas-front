import {Component, Injector, OnInit} from '@angular/core';
import {CrudListComponent} from '../../framework/directives/crud.list.component';
import {Categoria} from '../categoria';
import {CategoriaService} from '../categoria.service';
import {CategoriaInput} from '../categoriaInput';
import {Utils} from '../../framework/util/utils';
import {FieldType} from '../../framework/component/filter-custom/fieldType';
import {FormGroup} from '@angular/forms';
import {CategoriaOutput} from '../categoriaOutput';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.list.component.html',
  styleUrls: ['./categoria.list.component.css']
})
export class CategoriaListComponent extends CrudListComponent<Categoria, CategoriaInput, CategoriaOutput, string> implements OnInit {

  constructor(protected categoriaService: CategoriaService,
              protected injector: Injector) {
    super(categoriaService, injector, 'categoria/form');
  }

  filter($event: FormGroup) {
    if ($event) {
      const resultFilter = $event.getRawValue();
      if (Utils.isNotBlank(resultFilter.codigo) || Utils.isNotBlank(resultFilter.descricao)) {
        this.objectInput.codigo = resultFilter.codigo;
        this.objectInput.descricao = resultFilter.descricao;
        this.findAllByFilter();
      }
    }
  }

  getFilterFields() {
    this.filterFields.push(
      {
        field: 'codigo',
        label: 'Código',
        type: FieldType.NUMBER,
        size: 'col-md-3'
      },
      {
        field: 'descricao',
        label: 'Descrição',
        type: FieldType.TEXT,
        size: 'col-md-9'
      }
    );
  }

  getColumnsGrid() {
    this.columns = [
      {field: 'codigo', header: 'Código'},
      {field: 'descricao', header: 'Descrição'}
    ];
  }
}
