import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoriaListComponent} from './list/categoria.list.component';
import {CategoriaService} from './categoria.service';
import {CategoriaFormComponent} from './form/categoria.form.component';
import {TableModule} from 'primeng/table';
import {AccordionModule} from 'primeng/accordion';
import {ButtonModule} from 'primeng/button';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TableCustomModule} from '../framework/component/table-custom/table-custom.module';
import {ValidationModule} from '../framework/validation/validation.module';
import {OnlyNumberModule} from '../framework/directives/onlyNumber/onlyNumber.module';
import {InputTextModule} from 'primeng/inputtext';
import {SalvarModule} from '../framework/component/shared-button/salvar/salvar.module';
import {CancelarModule} from '../framework/component/shared-button/cancelar/cancelar.module';
import {FilterCustomModule} from '../framework/component/filter-custom/filter-custom.module';
import {FormCustomModule} from '../framework/component/form-custom/form-custom.module';
import {FormErrorModule} from '../framework/formError/formError.module';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    AccordionModule,
    ButtonModule,
    RouterModule,
    FormsModule,
    TableCustomModule,
    ValidationModule,
    OnlyNumberModule,
    InputTextModule,
    SalvarModule,
    CancelarModule,
    FilterCustomModule,
    FormCustomModule,
    ReactiveFormsModule,
    FormErrorModule
  ],
  declarations: [
    CategoriaListComponent,
    CategoriaFormComponent
  ],
  exports: [
    CategoriaListComponent,
    CategoriaFormComponent
  ],
  providers: [
    CategoriaService
  ]
})
export class CategoriaModule {
}
