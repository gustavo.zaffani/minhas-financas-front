import {PageRequest} from '../framework/dto/pageRequest';

export class CategoriaInput {
  codigo: number;
  descricao: string;
  pageRequest: PageRequest
}
