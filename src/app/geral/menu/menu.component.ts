import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {MenuItem} from 'primeng/api';
import {TranslateService} from '@ngx-translate/core';
import {PREFIX} from '../../framework/util/utils';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent implements OnInit {

  items: MenuItem[];

  constructor(private translate: TranslateService) {
  }

  ngOnInit(): void {
    this.items = [
      {
        label: this.translate.instant(`${PREFIX}menu_visao_geral`),
        icon: 'fa fa-search-dollar'
      },
      {
        label: this.translate.instant(`${PREFIX}menu_pagamentos`),
        icon: 'fa fa-hand-holding-usd'
      },
      {
        label: this.translate.instant(`${PREFIX}menu_despesas`),
        icon: 'pi pi-pw pi-file',
        items: [
          {
            label: this.translate.instant(`${PREFIX}menu_contas_fixas`),
            icon: 'pi pi-fw pi-plus',
            routerLink: 'despesa-fixa'
          },
          {
            label: this.translate.instant(`${PREFIX}menu_gastos_diarios`),
            icon: 'pi pi-fw pi-external-link',
            routerLink: 'despesa-variavel'
          },
        ]
      },
      {
        label: this.translate.instant(`${PREFIX}menu_cartoes`),
        icon: 'fa fa-credit-card',
        items: [
          {
            label: this.translate.instant(`${PREFIX}menu_cartoes_cadastro`),
            icon: 'fa fa-plus',
            routerLink: 'cartao'
          },
          {
            label: this.translate.instant(`${PREFIX}menu_cartoes_fatura`),
            icon: 'fa fa-file-invoice-dollar'
          }
        ]
      },
      {
        label: this.translate.instant(`${PREFIX}menu_cadastros`),
        icon: 'fa fa-pen',
        items: [
          {
            label: this.translate.instant(`${PREFIX}menu_categorias`),
            icon: 'fa fa-tasks',
            routerLink: 'categoria'
          },
          {
            label: this.translate.instant(`${PREFIX}menu_usuarios`),
            icon: 'fa fa-user',
            routerLink: 'usuario'
          },
          {
            label: this.translate.instant(`${PREFIX}menu_pessoas`),
            icon: 'fa fa-user-plus',
            routerLink: 'pessoa'
          }
        ]
      },
      {
        label: this.translate.instant(`${PREFIX}menu_relatorios`),
        icon: 'pi pi-fw pi-cog'
      }
    ];
  }
}
