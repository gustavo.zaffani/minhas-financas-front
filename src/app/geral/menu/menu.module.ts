import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PanelMenuModule} from 'primeng/panelmenu';
import {MenuComponent} from './menu.component';

@NgModule({
  imports: [
    CommonModule,
    PanelMenuModule
  ],
  declarations: [
    MenuComponent
  ],
  exports: [
    MenuComponent
  ]
})
export class MenuModule {}
