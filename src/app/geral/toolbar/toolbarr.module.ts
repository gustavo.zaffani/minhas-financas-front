import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToolbarComponent} from './toolbar.component';
import {ToolbarModule} from 'primeng/toolbar';
import {RouterModule} from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        ToolbarModule,
        RouterModule
    ],
  declarations: [
    ToolbarComponent
  ],
  exports: [
    ToolbarComponent
  ]
})
export class ToolbarrModule {
}
