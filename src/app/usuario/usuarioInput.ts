import {PageRequest} from '../framework/dto/pageRequest';

export class UsuarioInput {
  nome: string;
  usuario: string;
  email: string;
  pageRequest: PageRequest;
}
