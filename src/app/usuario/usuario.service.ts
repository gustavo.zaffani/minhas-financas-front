import {Injectable} from '@angular/core';
import {CrudService} from '../framework/service/crud.service';
import {Usuario} from './usuario';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {UsuarioInput} from './usuarioInput';
import {UsuarioOutput} from './usuarioOutput';

@Injectable()
export class UsuarioService extends CrudService<Usuario, UsuarioInput, UsuarioOutput, string> {

  constructor(http: HttpClient) {
    super(`${environment.api_url}usuario/`, http);
  }
}
