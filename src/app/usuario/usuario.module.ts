import {NgModule} from '@angular/core';
import {UsuarioListComponent} from './list/usuario.list.component';
import {UsuarioFormComponent} from './form/usuario.form.component';
import {CommonModule} from '@angular/common';
import {UsuarioService} from './usuario.service';
import {FilterCustomModule} from '../framework/component/filter-custom/filter-custom.module';
import {TableCustomModule} from '../framework/component/table-custom/table-custom.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SalvarModule} from '../framework/component/shared-button/salvar/salvar.module';
import {CancelarModule} from '../framework/component/shared-button/cancelar/cancelar.module';
import {FormCustomModule} from '../framework/component/form-custom/form-custom.module';
import {FormErrorModule} from '../framework/formError/formError.module';
import {InputTextModule} from 'primeng/inputtext';
import {CalendarModule} from 'primeng/calendar';

@NgModule({
  imports: [
    CommonModule,
    FilterCustomModule,
    TableCustomModule,
    FormsModule,
    SalvarModule,
    CancelarModule,
    FormCustomModule,
    FormErrorModule,
    ReactiveFormsModule,
    InputTextModule,
    CalendarModule
  ],
  declarations: [
    UsuarioListComponent,
    UsuarioFormComponent
  ],
  exports: [
    UsuarioListComponent,
    UsuarioFormComponent
  ],
  providers: [
    UsuarioService
  ]
})
export class UsuarioModule {
}
