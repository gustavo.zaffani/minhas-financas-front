import {Component, Injector, OnInit} from '@angular/core';
import {CrudListComponent} from '../../framework/directives/crud.list.component';
import {Usuario} from '../usuario';
import {UsuarioService} from '../usuario.service';
import {UsuarioInput} from '../usuarioInput';
import {FormGroup} from '@angular/forms';
import {FieldType} from '../../framework/component/filter-custom/fieldType';
import {Utils} from '../../framework/util/utils';
import {UsuarioOutput} from '../usuarioOutput';

@Component({
  selector: 'app-usuario-list',
  templateUrl: './usuario.list.component.html',
  styleUrls: ['./usuario.list.component.css']
})
export class UsuarioListComponent extends CrudListComponent<Usuario, UsuarioInput, UsuarioOutput, string> implements OnInit {

  constructor(protected usuarioService: UsuarioService,
              protected injector: Injector) {
    super(usuarioService, injector, 'usuario/form');
    this.createListPrefix('usuario');
  }

  getFilterFields() {
    this.filterFields.push(
      {
        field: 'nome',
        label: this.translate('nome'),
        type: FieldType.TEXT,
        size: 'col-md-4'
      },
      {
        field: 'usuario',
        label: this.translate('user'),
        type: FieldType.TEXT,
        size: 'col-md-4'
      },
      {
        field: 'email',
        label: this.translate('email'),
        type: FieldType.TEXT,
        size: 'col-md-4'
      }
    );
  }

  getColumnsGrid() {
    this.columns = [
      {
        field: 'nome',
        header: this.translate('nome')
      },
      {
        field: 'usuario',
        header: this.translate('user')
      },
      {
        field: 'email',
        header: this.translate('email')
      },
      {
        field: 'dtNascimento',
        header: this.translate('data_nascimento')
      }
    ];
  }

  filter($event: FormGroup) {
    if ($event) {
      const resultFilter = $event.getRawValue();
      if (Utils.isNotBlank(resultFilter.email)
        || Utils.isNotBlank(resultFilter.nome)
        || Utils.isNotBlank(resultFilter.usuario)) {
        this.objectInput.email = resultFilter.email;
        this.objectInput.nome = resultFilter.nome;
        this.objectInput.usuario = resultFilter.usuario;
        this.findAllByFilter();
      }
    }
  }
}
