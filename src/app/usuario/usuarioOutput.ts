import {Usuario} from './usuario';

export class UsuarioOutput {
  contents: Usuario[];
  totalElements: number;
}
