import {Component, Injector} from '@angular/core';
import {CrudFormComponent} from '../../framework/directives/crud.form.component';
import {Usuario} from '../usuario';
import {UsuarioInput} from '../usuarioInput';
import {UsuarioOutput} from '../usuarioOutput';
import {UsuarioService} from '../usuario.service';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-usuario-form',
  templateUrl: './usuario.form.component.html',
  styleUrls: ['./usuario.form.component.css']
})
export class UsuarioFormComponent extends CrudFormComponent<Usuario, UsuarioInput, UsuarioOutput, string> {

  constructor(protected usuarioService: UsuarioService,
              protected injector: Injector) {
    super(usuarioService, injector, '/usuario');
    this.getFormGroup();
    this.createFormPrefix('usuario');
  }

  private getFormGroup() {
    this.formGroup = new FormBuilder().group({
      id: [null],
      nome: [null, Validators.required],
      usuario: [null, Validators.required],
      senha: [null, Validators.required],
      email: [null],
      dtNascimento: [null]
    });
  }
}
