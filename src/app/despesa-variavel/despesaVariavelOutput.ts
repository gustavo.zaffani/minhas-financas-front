import {DespesaVariavel} from './despesaVariavel';

export class DespesaVariavelOutput {
  contents: DespesaVariavel[];
  totalElements: number;
}
