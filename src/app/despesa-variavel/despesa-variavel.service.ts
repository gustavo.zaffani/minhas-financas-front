import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {CrudService} from '../framework/service/crud.service';
import {DespesaVariavel} from './despesaVariavel';
import {DespesaVariavelInput} from './despesaVariavelInput';
import {DespesaVariavelOutput} from './despesaVariavelOutput';
import {Observable} from 'rxjs';

@Injectable()
export class DespesaVariavelService extends CrudService<DespesaVariavel, DespesaVariavelInput, DespesaVariavelOutput, string> {

  constructor(http: HttpClient) {
    super(`${environment.api_url}despesa-variavel/`, http);
  }

  saveCustom(despesaVariavel: DespesaVariavel): Observable<DespesaVariavel> {
    return this.http.post<DespesaVariavel>(this.getUrl() + 'save-custom', despesaVariavel);
  }
}
