import {Component, Injector} from '@angular/core';
import {CrudFormComponent} from '../../framework/directives/crud.form.component';
import {DespesaVariavel} from '../despesaVariavel';
import {DespesaVariavelInput} from '../despesaVariavelInput';
import {DespesaVariavelOutput} from '../despesaVariavelOutput';
import {DespesaVariavelService} from '../despesa-variavel.service';
import {FormBuilder, Validators} from '@angular/forms';
import {Categoria} from '../../categoria/categoria';
import {CategoriaService} from '../../categoria/categoria.service';
import {SelectItem} from 'primeng/api';
import {EnumFormaPagamento} from '../enumFormaPagamento';
import {Cartao} from '../../cartao/cadastro/cartao';
import {CartaoService} from '../../cartao/cadastro/cartao.service';
import {FormUtil} from '../../framework/util/form.util';
import {TableColumn} from '../../framework/component/table-custom/tableColumn';
import {EnumFormatter} from '../../framework/formatters/enumFormatter';
import {EnumStatusParcela} from '../enumStatusParcela';
import {BadgeColor} from '../../framework/component/table-custom/badgeColor';
import {CurrencyFormatter} from '../../framework/formatters/currencyFormatter';
import {MessageUtil} from '../../framework/util/message.util';
import {PREFIX} from '../../framework/util/utils';
import {DespesaRateio} from '../despesaRateio';

@Component({
  selector: 'app-despesa-variavel-form',
  templateUrl: './despesa-variavel.form.component.html',
  styleUrls: ['./despesa-variavel.form.component.css']
})
export class DespesaVariavelFormComponent extends CrudFormComponent<DespesaVariavel, DespesaVariavelInput, DespesaVariavelOutput, string> {

  categoriaList: Categoria[];
  formaPagamentoDropdown: SelectItem[];
  cartaoList: Cartao[];
  columns: TableColumn[];
  columnsRateio: TableColumn[];
  showDialogEditParcela = false;
  showDialogEditRateio = false;
  rateioCurrent: DespesaRateio;

  constructor(protected despesaVariavelService: DespesaVariavelService,
              protected injector: Injector,
              private categoriaService: CategoriaService,
              private cartaoService: CartaoService) {
    super(despesaVariavelService, injector, '/despesa-variavel');
    this.getFormGroup();
    this.createFormPrefix('despesa_variavel');
    this.createDropdown();
    this.getColumnsGridParcela();
    this.getColumnsGridRateio();
  }

  public save() {
    if (this.formGroup.valid) {
      this.loaderService.display(true);
      this.despesaVariavelService.saveCustom(this.formGroup.getRawValue())
        .subscribe(e => {
          this.loaderService.display(false);
          MessageUtil.showMessageSuccess(this.messageService, this.translateService, 'message.saved.success');
          this.back();
        }, error => {
          console.error(error);
          this.loaderService.display(false);
          MessageUtil.showMessageError(this.messageService, this.translateService, 'message.saved.error');
        });
    } else {
      this.formGroup.markAllAsTouched();
    }
  }

  private getColumnsGridParcela() {
    this.columns = [
      {
        field: 'sequencia',
        header: this.translate('grid_parcelas_sequencia')
      },
      {
        field: 'dtVencimento',
        header: this.translate('grid_parcelas_data_vencimento')
      },
      {
        field: 'vlrParcela',
        header: this.translate('grid_parcelas_valor'),
        formatter: new CurrencyFormatter()
      },
      {
        field: 'statusParcela',
        header: this.translate('grid_parcelas_status'),
        badge: {
          badgeOptions: [
            {onlyResult: EnumStatusParcela.ATRASADO, badge: BadgeColor.RED},
            {onlyResult: EnumStatusParcela.PAGO, badge: BadgeColor.GREEN},
            {onlyResult: EnumStatusParcela.PENDENTE, badge: BadgeColor.BLUE}
          ]
        },
        formatter: new EnumFormatter(
          [
            {
              label: this.translateService.instant(`${PREFIX}enum_status_parcela.pago`),
              value: EnumStatusParcela.PAGO
            },
            {
              label: this.translateService.instant(`${PREFIX}enum_status_parcela.atrasado`),
              value: EnumStatusParcela.ATRASADO
            },
            {
              label: this.translateService.instant(`${PREFIX}enum_status_parcela.pendente`),
              value: EnumStatusParcela.PENDENTE
            }
          ]
        )
      }
    ];
  }

  public initializeValues() {
    this.formGroup.get('formaPagamento').setValue(EnumFormaPagamento.DINHEIRO);
    this.formGroup.get('dtDespesa').setValue(new Date().toLocaleDateString());
    this.onSelectFormaPagamento();
  }

  public postEdit() {
    this.onSelectFormaPagamento();
  }

  private createDropdown() {
    this.formaPagamentoDropdown = [
      {
        label: this.translateService.instant(`${PREFIX}enum_forma_pagamento.dinheiro`),
        value: EnumFormaPagamento.DINHEIRO
      },
      {
        label: this.translateService.instant(`${PREFIX}enum_forma_pagamento.boleto`),
        value: EnumFormaPagamento.BOLETO
      },
      {
        label: this.translateService.instant(`${PREFIX}enum_forma_pagamento.cartao`),
        value: EnumFormaPagamento.CARTAO
      }
    ];
  }

  private getFormGroup() {
    this.formGroup = new FormBuilder().group({
      id: [null],
      descricao: [null, Validators.required],
      dtCadastro: [null],
      dtDespesa: [null, Validators.required],
      formaPagamento: [null, Validators.required],
      categoria: [null],
      cartao: [null],
      qtdParcela: [null],
      statusDespesa: [null],
      vlrDespesa: [null, Validators.required],
      diaVencimento: [null],
      observacao: [null],
      parcelas: [null],
      rateios: [null]
    });
  }

  public findCategorias($event: any) {
    this.categoriaService.complete($event.query)
      .subscribe(e => {
        this.categoriaList = e;
      });
  }

  public findCartoes($event: any) {
    this.cartaoService.complete($event.query)
      .subscribe(e => {
        this.cartaoList = e;
      });
  }

  public onSelectFormaPagamento() {
    const formaPagamento = this.formGroup.get('formaPagamento').value;
    if (formaPagamento) {
      FormUtil.enableFields(['qtdParcela', 'diaVencimento', 'cartao'], this.formGroup);
      FormUtil.setValidatorRequiredFields(['qtdParcela', 'diaVencimento', 'cartao'], this.formGroup);

      if (formaPagamento === EnumFormaPagamento.DINHEIRO) {
        FormUtil.disableAndClearFields(['qtdParcela', 'diaVencimento', 'cartao'], this.formGroup);
      } else if (formaPagamento === EnumFormaPagamento.BOLETO) {
        FormUtil.disableAndClearFields(['cartao'], this.formGroup);
      } else if (formaPagamento === EnumFormaPagamento.CARTAO) {
        FormUtil.disableAndClearFields(['diaVencimento'], this.formGroup);
      }
    }
  }

  public fieldIsEnabled(field: string) {
    return !FormUtil.fieldIsDisabled(field, this.formGroup);
  }

  private getColumnsGridRateio() {
    this.columnsRateio = [
      {
        field: 'pessoa.nome',
        header: 'Pessoa'
      },
      {
        field: 'vlrRateio',
        header: 'Valor rateio',
        formatter: new CurrencyFormatter()
      }
    ]
  }

  public addRateio() {
    this.showDialogEditRateio = true;
  }

  public editRateio(rateio: any[]) {
    this.rateioCurrent = rateio[0];
    this.showDialogEditRateio = true;
  }

  public saveRateio(rateioToSave: DespesaRateio) {
    this.showDialogEditRateio = false;
    const rateios = this.formGroup.get('rateios').value;

    if (this.rateioCurrent) {
      const index = rateios.findIndex(rateio => rateio.id === this.rateioCurrent.id);
      rateios[index] = rateioToSave;
      this.rateioCurrent = null;
      return;
    }

    const index = rateios.findIndex(rateio => rateio.pessoa.id === rateioToSave.pessoa.id);
    if (index != -1) {
      rateios[index].vlrRateio += rateioToSave.vlrRateio;
      return;
    }

    rateios.push(rateioToSave);
  }

  public onCloseRateio() {
    this.rateioCurrent = null;
    this.showDialogEditRateio = false;
  }

  public onChangeVlrDespesa() {
    this.formGroup.get('rateios').patchValue(new Array());
  }

  public getMaxValueRateio() {
    const vlrDespesa = this.formGroup.get('vlrDespesa').value;
    const rateios = this.formGroup.get('rateios').value;
    if (rateios?.length > 0) {
      let totalRateado = 0;
      rateios.forEach(rateio => {
        totalRateado += rateio.vlrRateio;
      });
      return (vlrDespesa - totalRateado).toFixed(2);
    }
    return vlrDespesa?.toFixed(2);
  }

  deleteRateio(rateiosToDelete: any[]) {
    const rateios = this.formGroup.get('rateios').value;
    if (rateiosToDelete?.length > 0) {
      rateiosToDelete.forEach(rateioDelete => {
        const index = rateios.findIndex(rateio => rateio.id === rateioDelete.id);
        rateios.splice(index, 1);
      });
    }
  }
}

