import {EnumStatusDespesa} from './enumStatusDespesa';
import {Cartao} from '../cartao/cadastro/cartao';
import {Categoria} from '../categoria/categoria';
import {EnumFormaPagamento} from './enumFormaPagamento';
import {Parcela} from './parcela';
import {DespesaRateio} from './despesaRateio';

export class DespesaVariavel {
  id: string;
  descricao: string;
  dtCadastro: string;
  dtDespesa: string;
  formaPagamento: EnumFormaPagamento;
  categoria: Categoria;
  cartao: Cartao;
  qtdParcela: number;
  statusDespesa: EnumStatusDespesa;
  vlrDespesa: number;
  diaVencimento: number;
  observacao: string;
  parcelas: Parcela[];
  rateios: DespesaRateio[];
}
