import {AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DespesaRateio} from '../despesaRateio';
import {Pessoa} from '../../pessoa/pessoa';
import {PessoaService} from '../../pessoa/pessoa.service';
import {CurrencyFormatter} from '../../framework/formatters/currencyFormatter';

@Component({
  selector: 'app-despesa-rateio',
  templateUrl: './despesa-rateio.component.html',
  styleUrls: ['./despesa-rateio.component.css']
})
export class DespesaRateioComponent implements OnChanges {

  @Input() showDialog = false;
  @Input() maxValue: number;
  @Input() rateio: DespesaRateio;
  @Output() onClose: EventEmitter<any> = new EventEmitter();
  @Output() onSave: EventEmitter<DespesaRateio> = new EventEmitter();
  formGroup: FormGroup;
  pessoaList: Pessoa[];

  constructor(private pessoaService: PessoaService) {
    this.getFormGroup();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.showDialog?.currentValue == true) {
      if (this.rateio) {
        this.formGroup.patchValue(this.rateio);
      } else {
        if (this.maxValue) {
          this.formGroup.get('vlrRateio').setValue(Number(this.maxValue));
        }
      }
    }
  }

  private getFormGroup() {
    this.formGroup = new FormBuilder().group({
      id: [null],
      vlrRateio: [null, Validators.required],
      pessoa: [null, Validators.required],
      despesaVariavel: [null]
    });
  }

  close() {
    this.clearRateio();
    this.onClose.emit();
  }

  save() {
    if (this.formGroup.valid) {
      this.rateio = this.formGroup.getRawValue();
      this.onSave.emit(this.rateio);
      this.clearRateio();
    } else {
      this.formGroup.markAllAsTouched();
    }
  }

  findPessoas($event: any) {
    this.pessoaService.complete($event.query)
      .subscribe(e => {
        this.pessoaList = e;
      });
  }

  formatValue(value: number) {
    if (value) {
      return CurrencyFormatter.format(Number(value));
    }
    return '0';
  }

  clearRateio() {
    this.rateio = null;
    this.formGroup.reset();
  }
}

