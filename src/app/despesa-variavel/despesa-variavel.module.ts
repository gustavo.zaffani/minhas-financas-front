import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DespesaVariavelFormComponent} from './form/despesa-variavel.form.component';
import {DespesaVariavelListComponent} from './list/despesa-variavel.list.component';
import {DespesaVariavelService} from './despesa-variavel.service';
import {FilterCustomModule} from '../framework/component/filter-custom/filter-custom.module';
import {TableCustomModule} from '../framework/component/table-custom/table-custom.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FormErrorModule} from '../framework/formError/formError.module';
import {SalvarModule} from '../framework/component/shared-button/salvar/salvar.module';
import {CancelarModule} from '../framework/component/shared-button/cancelar/cancelar.module';
import {CaracterCoutingModule} from '../framework/component/caracter-couting/caracter-couting.module';
import {InputTextModule} from 'primeng/inputtext';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {CalendarModule} from 'primeng/calendar';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {DropdownModule} from 'primeng/dropdown';
import {OnlyNumberModule} from '../framework/directives/onlyNumber/onlyNumber.module';
import {AccordionModule} from 'primeng/accordion';
import {DialogModule} from 'primeng/dialog';
import {DespesaRateioComponent} from './despesa-rateio/despesa-rateio.component';

@NgModule({
  imports: [
    CommonModule,
    FilterCustomModule,
    TableCustomModule,
    ReactiveFormsModule,
    FormErrorModule,
    SalvarModule,
    CancelarModule,
    CaracterCoutingModule,
    InputTextModule,
    CurrencyMaskModule,
    CalendarModule,
    AutoCompleteModule,
    DropdownModule,
    OnlyNumberModule,
    AccordionModule,
    DialogModule
  ],
  declarations: [
    DespesaVariavelFormComponent,
    DespesaVariavelListComponent,
    DespesaRateioComponent
  ],
  exports: [
    DespesaVariavelFormComponent,
    DespesaVariavelListComponent,
    DespesaRateioComponent
  ],
  providers: [
    DespesaVariavelService
  ]
})
export class DespesaVariavelModule {

}
