import {DespesaVariavel} from './despesaVariavel';
import {EnumStatusParcela} from './enumStatusParcela';

export class Parcela {
  id: string;
  sequencia: number;
  dtVencimento: string;
  vlrParcela: number;
  statusParcela: EnumStatusParcela;
  despesaVariavel: DespesaVariavel;
}
