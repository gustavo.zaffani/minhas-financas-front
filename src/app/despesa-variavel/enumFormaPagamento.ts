export enum EnumFormaPagamento {
  DINHEIRO = 'DINHEIRO',
  CARTAO = 'CARTAO',
  BOLETO = 'BOLETO'
}
