import {Pessoa} from '../pessoa/pessoa';
import {DespesaVariavel} from './despesaVariavel';

export class DespesaRateio {
  id: string;
  vlrRateio: number;
  pessoa: Pessoa;
  despesaVariavel: DespesaVariavel;
}
