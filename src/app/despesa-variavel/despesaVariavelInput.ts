import {EnumStatusDespesa} from './enumStatusDespesa';
import {EnumFormaPagamento} from './enumFormaPagamento';
import {PageRequest} from '../framework/dto/pageRequest';

export class DespesaVariavelInput {
  descricao: string;
  vlrDespesa: number;
  statusDespesa: EnumStatusDespesa;
  formaPagamento: EnumFormaPagamento;
  pageRequest: PageRequest;
}
