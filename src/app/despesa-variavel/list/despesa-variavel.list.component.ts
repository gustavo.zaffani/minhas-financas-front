import {Component, Injector} from '@angular/core';
import {CrudListComponent} from '../../framework/directives/crud.list.component';
import {DespesaVariavel} from '../despesaVariavel';
import {DespesaVariavelInput} from '../despesaVariavelInput';
import {DespesaVariavelOutput} from '../despesaVariavelOutput';
import {DespesaVariavelService} from '../despesa-variavel.service';
import {FormGroup} from '@angular/forms';
import {PREFIX, Utils} from '../../framework/util/utils';
import {FieldType} from '../../framework/component/filter-custom/fieldType';
import {EnumStatusDespesa} from '../enumStatusDespesa';
import {EnumFormaPagamento} from '../enumFormaPagamento';
import {BadgeColor} from '../../framework/component/table-custom/badgeColor';
import {EnumFormatter} from '../../framework/formatters/enumFormatter';
import {CurrencyFormatter} from '../../framework/formatters/currencyFormatter';

@Component({
  selector: 'app-despesa-variavel-list',
  templateUrl: './despesa-variavel.list.component.html',
  styleUrls: ['./despesa-variavel.list.component.css']
})
export class DespesaVariavelListComponent extends CrudListComponent<DespesaVariavel, DespesaVariavelInput, DespesaVariavelOutput, string> {

  constructor(protected despesaVariavelService: DespesaVariavelService,
              protected injector: Injector) {
    super(despesaVariavelService, injector, 'despesa-variavel/form');
    this.createListPrefix('despesa_variavel');
  }

  filter($event: FormGroup) {
    if ($event) {
      const resultFilter = $event.getRawValue();
      if (Utils.isNotBlank(resultFilter.descricao) ||
        Utils.isNotBlank(resultFilter.vlrDespesa) ||
        Utils.isNotBlank(resultFilter.formaPagamento) ||
        Utils.isNotBlank(resultFilter.statusDespesa)) {
        this.objectInput.descricao = resultFilter.descricao;
        this.objectInput.vlrDespesa = resultFilter.vlrDespesa;
        this.objectInput.formaPagamento = resultFilter.formaPagamento;
        this.objectInput.statusDespesa = resultFilter.statusDespesa;
        this.findAllByFilter();
      }
    }
  }

  getFilterFields() {
    this.filterFields.push(
      {
        field: 'descricao',
        label: this.translate('descricao'),
        type: FieldType.TEXT,
        size: 'col-md-12'
      },
      {
        field: 'vlrDespesa',
        label: this.translate('valor_despesa'),
        type: FieldType.MONEY,
        size: 'col-md-4'
      },
      {
        field: 'statusDespesa',
        label: this.translate('status'),
        type: FieldType.SELECT,
        size: 'col-md-4',
        options: [
          {
            label: this.translateService.instant(`${PREFIX}enum_status_despesa.pendente`),
            value: EnumStatusDespesa.PENDENTE
          },
          {
            label: this.translateService.instant(`${PREFIX}enum_status_despesa.parcialmente_pago`),
            value: EnumStatusDespesa.PARCIALMENTE_PAGO
          },
          {
            label: this.translateService.instant(`${PREFIX}enum_status_despesa.totalmente_pago`),
            value: EnumStatusDespesa.TOTALMENTE_PAGO
          }
        ]
      },
      {
        field: 'formaPagamento',
        label: this.translate('forma_pagamento'),
        type: FieldType.SELECT,
        size: 'col-md-4',
        options: [
          {
            label: this.translateService.instant(`${PREFIX}enum_forma_pagamento.boleto`),
            value: EnumFormaPagamento.BOLETO
          },
          {
            label: this.translateService.instant(`${PREFIX}enum_forma_pagamento.dinheiro`),
            value: EnumFormaPagamento.DINHEIRO
          },
          {
            label: this.translateService.instant(`${PREFIX}enum_forma_pagamento.cartao`),
            value: EnumFormaPagamento.CARTAO
          }
        ]
      }
    );
  }

  getColumnsGrid() {
    this.columns = [
      {
        field: 'descricao',
        header: this.translate('descricao')
      },
      {
        field: 'formaPagamento',
        header: this.translate('forma_pagamento'),
        formatter: new EnumFormatter(
          [
            {
              label: this.translateService.instant(`${PREFIX}enum_forma_pagamento.boleto`),
              value: EnumFormaPagamento.BOLETO
            },
            {
              label: this.translateService.instant(`${PREFIX}enum_forma_pagamento.dinheiro`),
              value: EnumFormaPagamento.DINHEIRO
            },
            {
              label: this.translateService.instant(`${PREFIX}enum_forma_pagamento.cartao`),
              value: EnumFormaPagamento.CARTAO
            }
          ]
        )
      },
      {
        field: 'dtDespesa',
        header: this.translate('data_despesa')
      },
      {
        field: 'vlrDespesa',
        header: this.translate('valor_despesa'),
        formatter: new CurrencyFormatter()
      },
      {
        field: 'statusDespesa',
        header: this.translate('status'),
        badge: {
          badgeOptions: [
            {onlyResult: EnumStatusDespesa.TOTALMENTE_PAGO, badge: BadgeColor.GREEN},
            {onlyResult: EnumStatusDespesa.PARCIALMENTE_PAGO, badge: BadgeColor.BLUE},
            {onlyResult: EnumStatusDespesa.PENDENTE, badge: BadgeColor.RED}
          ]
        },
        formatter: new EnumFormatter(
          [
            {
              label: this.translateService.instant(`${PREFIX}enum_status_despesa.parcialmente_pago`),
              value: EnumStatusDespesa.PARCIALMENTE_PAGO
            },
            {
              label: this.translateService.instant(`${PREFIX}enum_status_despesa.totalmente_pago`),
              value: EnumStatusDespesa.TOTALMENTE_PAGO
            },
            {
              label: this.translateService.instant(`${PREFIX}enum_status_despesa.pendente`),
              value: EnumStatusDespesa.PENDENTE
            }
          ]
        )
      },
    ];
  }
}
