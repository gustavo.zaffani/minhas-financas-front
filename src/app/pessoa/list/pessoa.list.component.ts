import {Component, Injector, OnInit} from '@angular/core';
import {CrudListComponent} from '../../framework/directives/crud.list.component';
import {Pessoa} from '../pessoa';
import {PessoaInput} from '../pessoaInput';
import {PessoaOutput} from '../pessoaOutput';
import {PessoaService} from '../pessoa.service';
import {FormGroup} from '@angular/forms';
import {Utils} from '../../framework/util/utils';
import {FieldType} from '../../framework/component/filter-custom/fieldType';

@Component({
  selector: 'app-pessoa-list',
  templateUrl: './pessoa.list.component.html',
  styleUrls: ['./pessoa.list.component.css']
})
export class PessoaListComponent extends CrudListComponent<Pessoa, PessoaInput, PessoaOutput, string> {

  constructor(protected pessoaService: PessoaService,
              protected injector: Injector) {
    super(pessoaService, injector, 'pessoa/form');
  }

  filter($event: FormGroup) {
    if ($event) {
      const resultFilter = $event.getRawValue();
      if (Utils.isNotBlank(resultFilter.codigo) || Utils.isNotBlank(resultFilter.nome)) {
        this.objectInput.codigo = resultFilter.codigo;
        this.objectInput.nome = resultFilter.nome;
        this.findAllByFilter();
      }
    }
  }

  getFilterFields() {
    this.filterFields.push(
      {
        field: 'codigo',
        label: 'Código',
        type: FieldType.NUMBER,
        size: 'col-md-3'
      },
      {
        field: 'nome',
        label: 'Nome',
        type: FieldType.TEXT,
        size: 'col-md-9'
      }
    );
  }

  getColumnsGrid() {
    this.columns = [
      {field: 'codigo', header: 'Código'},
      {field: 'nome', header: 'Nome'}
    ];
  }
}
