import {Injectable} from '@angular/core';
import {CrudService} from '../framework/service/crud.service';
import {Pessoa} from './pessoa';
import {PessoaInput} from './pessoaInput';
import {PessoaOutput} from './pessoaOutput';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class PessoaService extends CrudService<Pessoa, PessoaInput, PessoaOutput, string> {

  constructor(http: HttpClient) {
    super(`${environment.api_url}pessoa/`, http);
  }
}
