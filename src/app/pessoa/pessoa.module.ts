import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PessoaListComponent} from './list/pessoa.list.component';
import {PessoaFormComponent} from './form/pessoa.form.component';
import {PessoaService} from './pessoa.service';
import {FilterCustomModule} from '../framework/component/filter-custom/filter-custom.module';
import {TableCustomModule} from '../framework/component/table-custom/table-custom.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FormErrorModule} from '../framework/formError/formError.module';
import {InputTextModule} from 'primeng/inputtext';
import {OnlyNumberModule} from '../framework/directives/onlyNumber/onlyNumber.module';
import {SalvarModule} from '../framework/component/shared-button/salvar/salvar.module';
import {CancelarModule} from '../framework/component/shared-button/cancelar/cancelar.module';

@NgModule({
  imports: [
    CommonModule,
    FilterCustomModule,
    TableCustomModule,
    ReactiveFormsModule,
    FormErrorModule,
    InputTextModule,
    OnlyNumberModule,
    SalvarModule,
    CancelarModule
  ],
  declarations: [
    PessoaListComponent,
    PessoaFormComponent
  ],
  exports: [
    PessoaListComponent,
    PessoaFormComponent
  ],
  providers: [
    PessoaService
  ]
})
export class PessoaModule {}
