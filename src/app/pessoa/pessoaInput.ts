import {PageRequest} from '../framework/dto/pageRequest';

export class PessoaInput {
  codigo: number;
  nome: string;
  pageRequest: PageRequest
}
