import {Pessoa} from './pessoa';

export class PessoaOutput {
  contents: Pessoa[];
  totalElements: number;
}
