import {Component, Injector} from '@angular/core';
import {CrudFormComponent} from '../../framework/directives/crud.form.component';
import {Pessoa} from '../pessoa';
import {PessoaInput} from '../pessoaInput';
import {PessoaOutput} from '../pessoaOutput';
import {PessoaService} from '../pessoa.service';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-pessoa-form',
  templateUrl: './pessoa.form.component.html',
  styleUrls: ['./pessoa.form.component.css']
})
export class PessoaFormComponent extends CrudFormComponent<Pessoa, PessoaInput, PessoaOutput, string> {

  constructor(protected pessoaService: PessoaService,
              protected injector: Injector) {
    super(pessoaService, injector, '/pessoa');
    this.getFormGroup();
    this.createFormPrefix('pessoa');
  }

  private getFormGroup() {
    this.formGroup = new FormBuilder().group({
      id: [null],
      codigo: [null, Validators.required],
      nome: [null, Validators.required]
    });
  }
}
