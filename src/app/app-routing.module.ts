import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CategoriaListComponent} from './categoria/list/categoria.list.component';
import {CategoriaFormComponent} from './categoria/form/categoria.form.component';
import {UsuarioListComponent} from './usuario/list/usuario.list.component';
import {UsuarioFormComponent} from './usuario/form/usuario.form.component';
import {CartaoListComponent} from './cartao/cadastro/list/cartao.list.component';
import {CartaoFormComponent} from './cartao/cadastro/form/cartao.form.component';
import {NotFoundComponent} from './geral/notFound/notFound.component';
import {PessoaListComponent} from './pessoa/list/pessoa.list.component';
import {PessoaFormComponent} from './pessoa/form/pessoa.form.component';
import {DespesaFixaListComponent} from './despesa-fixa/list/despesa-fixa.list.component';
import {DespesaFixaFormComponent} from './despesa-fixa/form/despesa-fixa.form.component';
import {HomeComponent} from './home/home.component';
import {DespesaVariavelListComponent} from './despesa-variavel/list/despesa-variavel.list.component';
import {DespesaVariavelFormComponent} from './despesa-variavel/form/despesa-variavel.form.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'categoria', component: CategoriaListComponent},
  {path: 'categoria/form', component: CategoriaFormComponent},
  {path: 'categoria/form/:id', component: CategoriaFormComponent},
  {path: 'usuario', component: UsuarioListComponent},
  {path: 'usuario/form', component: UsuarioFormComponent},
  {path: 'usuario/form/:id', component: UsuarioFormComponent},
  {path: 'cartao', component: CartaoListComponent},
  {path: 'cartao/form', component: CartaoFormComponent},
  {path: 'cartao/form/:id', component: CartaoFormComponent},
  {path: 'pessoa', component: PessoaListComponent},
  {path: 'pessoa/form', component: PessoaFormComponent},
  {path: 'pessoa/form/:id', component: PessoaFormComponent},
  {path: 'despesa-fixa', component: DespesaFixaListComponent},
  {path: 'despesa-fixa/form', component: DespesaFixaFormComponent},
  {path: 'despesa-fixa/form/:id', component: DespesaFixaFormComponent},
  {path: 'despesa-variavel', component: DespesaVariavelListComponent},
  {path: 'despesa-variavel/form', component: DespesaVariavelFormComponent},
  {path: 'despesa-variavel/form/:id', component: DespesaVariavelFormComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
