import {Component, Injector} from '@angular/core';
import {CrudListComponent} from '../../../framework/directives/crud.list.component';
import {Cartao} from '../cartao';
import {CartaoInput} from '../cartaoInput';
import {CartaoOutput} from '../cartaoOutput';
import {CartaoService} from '../cartao.service';
import {FormGroup} from '@angular/forms';
import {Utils} from '../../../framework/util/utils';
import {FieldType} from '../../../framework/component/filter-custom/fieldType';
import {PipeFormatter} from '../../../framework/component/table-custom/pipeFormatter';
import {CurrencyFormatter} from '../../../framework/formatters/currencyFormatter';

@Component({
  selector: 'app-cartao-list',
  templateUrl: './cartao.list.component.html',
  styleUrls: ['./cartao.list.component.css']
})
export class CartaoListComponent extends CrudListComponent<Cartao, CartaoInput, CartaoOutput, string> {

  constructor(protected cartaoService: CartaoService,
              protected injector: Injector) {
    super(cartaoService, injector, 'cartao/form');
  }

  filter($event: FormGroup) {
    if ($event) {
      const resultFilter = $event.getRawValue();
      if (Utils.isNotBlank(resultFilter.codigo) ||
        Utils.isNotBlank(resultFilter.descricao) ||
        Utils.isNotBlank(resultFilter.dtVencimento) ||
        Utils.isNotBlank(resultFilter.melhorDataCompra) ||
        Utils.isNotBlank(resultFilter.limite)) {
        this.objectInput.codigo = resultFilter.codigo;
        this.objectInput.descricao = resultFilter.descricao;
        this.objectInput.dtVencimento = resultFilter.dtVencimento;
        this.objectInput.melhorDataCompra = resultFilter.melhorDataCompra;
        this.objectInput.limite = resultFilter.limite;
        this.findAllByFilter();
      }
    }
  }

  getFilterFields() {
    this.filterFields.push(
      {
        field: 'codigo',
        label: 'Código',
        type: FieldType.NUMBER,
        size: 'col-md-3'
      },
      {
        field: 'descricao',
        label: 'Descrição',
        type: FieldType.TEXT,
        size: 'col-md-9'
      },
      {
        field: 'dtVencimento',
        label: 'Data de vencimento',
        type: FieldType.NUMBER,
        size: 'col-md-4'
      },
      {
        field: 'melhorDataCompra',
        label: 'Melhor data',
        type: FieldType.NUMBER,
        size: 'col-md-4'
      },
      {
        field: 'limite',
        label: 'Limite',
        type: FieldType.MONEY,
        size: 'col-md-4'
      }
    );
  }

  getColumnsGrid() {
    this.columns = [
      {
        field: 'codigo',
        header: 'Código'
      },
      {
        field: 'descricao',
        header: 'Descrição'
      },
      {
        field: 'dtVencimento',
        header: 'Data de vencimento'
      },
      {
        field: 'melhorDataCompra',
        header: 'Melhor data'
      },
      {
        field: 'limite',
        header: 'Limite',
        formatter: new CurrencyFormatter()
      }
    ];
  }

}
