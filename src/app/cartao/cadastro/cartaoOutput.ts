import {Cartao} from './cartao';

export class CartaoOutput {
  contents: Cartao[];
  totalElements: number;
}
