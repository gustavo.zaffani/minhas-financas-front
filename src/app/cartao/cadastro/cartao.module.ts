import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CartaoListComponent} from './list/cartao.list.component';
import {CartaoFormComponent} from './form/cartao.form.component';
import {FilterCustomModule} from '../../framework/component/filter-custom/filter-custom.module';
import {TableCustomModule} from '../../framework/component/table-custom/table-custom.module';
import {CartaoService} from './cartao.service';
import {FormCustomModule} from '../../framework/component/form-custom/form-custom.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FormErrorModule} from '../../framework/formError/formError.module';
import {SalvarModule} from '../../framework/component/shared-button/salvar/salvar.module';
import {CancelarModule} from '../../framework/component/shared-button/cancelar/cancelar.module';
import {InputTextModule} from 'primeng/inputtext';
import {OnlyNumberModule} from '../../framework/directives/onlyNumber/onlyNumber.module';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {DropdownModule} from 'primeng/dropdown';

@NgModule({
  imports: [
    CommonModule,
    FilterCustomModule,
    TableCustomModule,
    FormCustomModule,
    ReactiveFormsModule,
    FormErrorModule,
    SalvarModule,
    CancelarModule,
    InputTextModule,
    OnlyNumberModule,
    CurrencyMaskModule,
    DropdownModule
  ],
  declarations: [
    CartaoListComponent,
    CartaoFormComponent
  ],
  exports: [
    CartaoListComponent,
    CartaoFormComponent
  ],
  providers: [
    CartaoService
  ]
})
export class CartaoModule {

}
