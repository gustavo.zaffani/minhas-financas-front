export class Cartao {
  id: string;
  codigo: number;
  descricao: string;
  dtVencimento: number;
  melhorDataCompra: number;
  limite: number;
  limiteDisponivel: number;
}
