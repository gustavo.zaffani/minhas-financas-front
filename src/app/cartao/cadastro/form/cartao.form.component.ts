import {Component, Injector} from '@angular/core';
import {CrudFormComponent} from '../../../framework/directives/crud.form.component';
import {Cartao} from '../cartao';
import {CartaoInput} from '../cartaoInput';
import {CartaoOutput} from '../cartaoOutput';
import {CartaoService} from '../cartao.service';
import {FormBuilder, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/api';
import {EnumTipoCartao} from '../enumTipoCartao';
import {FormUtil} from '../../../framework/util/form.util';
import {PREFIX} from '../../../framework/util/utils';

@Component({
  selector: 'app-cartao-form',
  templateUrl: './cartao.form.component.html',
  styleUrls: ['./cartao.form.component.css']
})
export class CartaoFormComponent extends CrudFormComponent<Cartao, CartaoInput, CartaoOutput, string> {

  tipoCartaoDropdown: SelectItem[];

  constructor(protected cartaoService: CartaoService,
              protected injector: Injector) {
    super(cartaoService, injector, '/cartao');
    this.getFormGroup();
    this.getTipoCartao();
    this.createFormPrefix('cartao');
  }

  preOnInit() {
    this.formGroup.get('tipoCartao').setValue(EnumTipoCartao.CREDITO);
  }

  postEdit() {
    this.onSelectTipoCartao();
  }

  private getFormGroup() {
    this.formGroup = new FormBuilder().group({
      id: [null],
      codigo: [null, Validators.required],
      descricao: [null, Validators.required],
      tipoCartao: [null],
      dtVencimento: [null, Validators.required],
      melhorDataCompra: [null, Validators.required],
      limite: [null, Validators.required]
    });
  }

  private getTipoCartao() {
    this.tipoCartaoDropdown = [
      {label: this.translateService.instant(`${PREFIX}cartao_tipo_cartao_credito`), value: EnumTipoCartao.CREDITO},
      {label: this.translateService.instant(`${PREFIX}cartao_tipo_cartao_debito`), value: EnumTipoCartao.DEBITO},
    ];
  }

  public onSelectTipoCartao() {
    if (this.formGroup.get('tipoCartao').value === EnumTipoCartao.CREDITO) {
      FormUtil.enableFields(['dtVencimento', 'melhorDataCompra', 'limite'], this.formGroup);
    } else {
      FormUtil.disableAndClearFields(['dtVencimento', 'melhorDataCompra', 'limite'], this.formGroup);
      FormUtil.clearValidatorsFields(['dtVencimento', 'melhorDataCompra', 'limite'], this.formGroup);
    }
  }

  public fieldIsEnabled(field: string) {
    return !FormUtil.fieldIsDisabled(field, this.formGroup);
  }

}
