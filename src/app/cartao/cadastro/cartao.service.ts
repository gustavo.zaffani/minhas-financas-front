import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {CrudService} from '../../framework/service/crud.service';
import {Cartao} from './cartao';
import {CartaoInput} from './cartaoInput';
import {CartaoOutput} from './cartaoOutput';
import {Injectable} from '@angular/core';

@Injectable()
export class CartaoService extends CrudService<Cartao, CartaoInput, CartaoOutput, string> {

  constructor(http: HttpClient) {
    super(`${environment.api_url}cartao/`, http);
  }
}
