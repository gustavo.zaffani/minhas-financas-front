import {PageRequest} from '../../framework/dto/pageRequest';

export class CartaoInput {
  codigo: number;
  descricao: string;
  dtVencimento: number;
  melhorDataCompra: number;
  limite: number;
  pageRequest: PageRequest
}
