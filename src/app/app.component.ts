import {Component, OnInit} from '@angular/core';
import {PrimeNGConfig} from 'primeng/api';
import {TranslateService} from '@ngx-translate/core';
import {Subject, Subscription} from 'rxjs';
import {NavigationCancel, NavigationEnd, NavigationStart, Router} from '@angular/router';
import {LoaderService} from './framework/loader/loader.service';

export let translationsWasLoaded = new Subject<boolean>();

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'minhas-financas-front';
  subscription: Subscription;

  constructor(private config: PrimeNGConfig,
              private translateService: TranslateService,
              private router: Router,
              private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.translatePrimeNG('pt-BR');
  }

  translatePrimeNG(lang: string) {
    this.translateService.use(lang);
    this.translateService.get('translations.primeng').subscribe(res => {
      this.config.setTranslation(res);
    });
  }

  buildSubscriptionEvent() {
    this.subscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.loaderService.display(true);
      } else if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
        this.loaderService.display(false);
      }
    });
  }
}
