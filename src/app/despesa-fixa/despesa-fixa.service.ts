import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {CrudService} from '../framework/service/crud.service';
import {DespesaFixa} from './despesaFixa';
import {DespesaFixaInput} from './despesaFixaInput';
import {DespesaFixaOutput} from './despesaFixaOutput';

@Injectable()
export class DespesaFixaService extends CrudService<DespesaFixa, DespesaFixaInput, DespesaFixaOutput, string> {

  constructor(http: HttpClient) {
    super(`${environment.api_url}despesa-fixa/`, http);
  }
}
