import {Component, Injector} from '@angular/core';
import {CrudListComponent} from '../../framework/directives/crud.list.component';
import {DespesaFixa} from '../despesaFixa';
import {DespesaFixaInput} from '../despesaFixaInput';
import {DespesaFixaOutput} from '../despesaFixaOutput';
import {DespesaFixaService} from '../despesa-fixa.service';
import {FormGroup} from '@angular/forms';
import {Utils} from '../../framework/util/utils';
import {FieldType} from '../../framework/component/filter-custom/fieldType';
import {CurrencyFormatter} from '../../framework/formatters/currencyFormatter';

@Component({
  selector: 'app-despesafixa-list',
  templateUrl: './despesa-fixa.list.component.html',
  styleUrls: ['./despesa-fixa.list.component.css']
})
export class DespesaFixaListComponent extends CrudListComponent<DespesaFixa, DespesaFixaInput, DespesaFixaOutput, string> {

  constructor(protected despesaFixaService: DespesaFixaService,
              protected injector: Injector) {
    super(despesaFixaService, injector, 'despesa-fixa/form');
  }

  filter($event: FormGroup) {
    if ($event) {
      const resultFilter = $event.getRawValue();
      if (Utils.isNotBlank(resultFilter.descricao) ||
        Utils.isNotBlank(resultFilter.diaVencimento) ||
        Utils.isNotBlank(resultFilter.vlrDespesa)) {
        this.objectInput.descricao = resultFilter.descricao;
        this.objectInput.diaVencimento = resultFilter.diaVencimento;
        this.objectInput.vlrDespesa = resultFilter.vlrDespesa;
        this.findAllByFilter();
      }
    }
  }

  getFilterFields() {
    this.filterFields.push(
      {
        field: 'descricao',
        label: 'Descrição',
        type: FieldType.TEXT,
        size: 'col-md-12'
      },
      {
        field: 'diaVencimento',
        label: 'Dia do vencimento',
        type: FieldType.NUMBER,
        size: 'col-md-6'
      },
      {
        field: 'vlrDespesa',
        label: 'Valor da despesa',
        type: FieldType.MONEY,
        size: 'col-md-6'
      }
    );
  }

  getColumnsGrid() {
    this.columns = [
      {
        field: 'descricao',
        header: 'Descrição'
      },
      {
        field: 'diaVencimento',
        header: 'Dia do vencimento'
      },
      {
        field: 'vlrDespesa',
        header: 'Valor da despesa',
        formatter: new CurrencyFormatter()
      }
    ];
  }
}
