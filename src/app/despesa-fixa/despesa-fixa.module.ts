import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DespesaFixaFormComponent} from './form/despesa-fixa.form.component';
import {DespesaFixaListComponent} from './list/despesa-fixa.list.component';
import {DespesaFixaService} from './despesa-fixa.service';
import {FilterCustomModule} from '../framework/component/filter-custom/filter-custom.module';
import {TableCustomModule} from '../framework/component/table-custom/table-custom.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FormErrorModule} from '../framework/formError/formError.module';
import {SalvarModule} from '../framework/component/shared-button/salvar/salvar.module';
import {CancelarModule} from '../framework/component/shared-button/cancelar/cancelar.module';
import {OnlyNumberModule} from '../framework/directives/onlyNumber/onlyNumber.module';
import {InputTextModule} from 'primeng/inputtext';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {CaracterCoutingModule} from '../framework/component/caracter-couting/caracter-couting.module';

@NgModule({
    imports: [
        CommonModule,
        FilterCustomModule,
        TableCustomModule,
        ReactiveFormsModule,
        FormErrorModule,
        SalvarModule,
        CancelarModule,
        OnlyNumberModule,
        InputTextModule,
        CurrencyMaskModule,
        CaracterCoutingModule
    ],
  declarations: [
    DespesaFixaFormComponent,
    DespesaFixaListComponent
  ],
  exports: [
    DespesaFixaFormComponent,
    DespesaFixaListComponent
  ],
  providers: [
    DespesaFixaService
  ]
})
export class DespesaFixaModule {}
