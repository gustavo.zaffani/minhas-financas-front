import {PageRequest} from '../framework/dto/pageRequest';

export class DespesaFixaInput {
  descricao: string;
  vlrDespesa: number;
  diaVencimento: number;
  pageRequest: PageRequest;
}
