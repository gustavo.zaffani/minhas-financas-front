import {DespesaFixa} from './despesaFixa';

export class DespesaFixaOutput {
  contents: DespesaFixa[];
  totalElements: number;
}
