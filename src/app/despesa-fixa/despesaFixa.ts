export class DespesaFixa {
  id: string;
  descricao: string;
  dtCadastro: string;
  vlrDespesa: number;
  diaVencimento: number;
  observacao: string;
}
