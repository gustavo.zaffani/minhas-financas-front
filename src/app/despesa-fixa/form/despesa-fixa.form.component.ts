import {Component, Injector} from '@angular/core';
import {DespesaFixa} from '../despesaFixa';
import {DespesaFixaInput} from '../despesaFixaInput';
import {DespesaFixaOutput} from '../despesaFixaOutput';
import {DespesaFixaService} from '../despesa-fixa.service';
import {FormBuilder, Validators} from '@angular/forms';
import {CrudFormComponent} from '../../framework/directives/crud.form.component';

@Component({
  selector: 'app-despesafixa-form',
  templateUrl: './despesa-fixa.form.component.html',
  styleUrls: ['./despesa-fixa.form.component.css']
})
export class DespesaFixaFormComponent extends CrudFormComponent<DespesaFixa, DespesaFixaInput, DespesaFixaOutput, string> {

  constructor(protected despesaFixaService: DespesaFixaService,
              protected injector: Injector) {
    super(despesaFixaService, injector, '/despesa-fixa');
    this.getFormGroup();
    this.createFormPrefix('despesa_fixa');
  }

  private getFormGroup() {
    this.formGroup = new FormBuilder().group({
      id: [null],
      descricao: [null, Validators.required],
      dtCadastro: [null],
      vlrDespesa: [null, Validators.required],
      diaVencimento: [null, Validators.required],
      observacao: [null],
    });
  }
}
